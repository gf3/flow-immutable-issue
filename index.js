/* @flow */

import { Record } from 'immutable';

type User = UserSpec & Record<UserSpec>;

const UserRecord = Record({
  id: ''
});

const goodUser: User = new UserRecord({ id: '666' });
console.log(goodUser.name); // Correctly fails

const badUser: LibUser = new UserRecord({ id: '666' });
console.log(badUser.name); // Incorrectly passes
